from matplotlib import pyplot


class Histogram(object):

    default_kwargs = {
        'bins': 256,
        'color': 'blue'
    }

    def update_kwargs(self, kwargs):
        for key, value in self.default_kwargs.items():
            if key not in kwargs:
                kwargs.update({key: value})
        return kwargs

    def build_histogram(self, values, **kwargs):
        self.update_kwargs(kwargs)
        pyplot.hist(values, **kwargs)
        pyplot.show()
