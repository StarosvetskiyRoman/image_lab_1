# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_sources/main_window.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets

from views.graphics_views import QGraphicsView


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1029, 692)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.graphics_view_2 = QGraphicsView(self.centralwidget, window=self, id=2)
        self.graphics_view_2.setObjectName("graphics_view_2")
        self.verticalLayout_2.addWidget(self.graphics_view_2)
        self.gridLayout.addLayout(self.verticalLayout_2, 1, 1, 1, 1)
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.graphics_view_1 = QGraphicsView(self.centralwidget, window=self, id=1)
        self.graphics_view_1.setObjectName("graphics_view_1")
        self.verticalLayout.addWidget(self.graphics_view_1)
        self.gridLayout.addLayout(self.verticalLayout, 1, 0, 1, 1)
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.graphics_view_3 = QGraphicsView(self.centralwidget, window=self, id=3)
        self.graphics_view_3.setObjectName("graphics_view_3")
        self.verticalLayout_3.addWidget(self.graphics_view_3)
        self.gridLayout.addLayout(self.verticalLayout_3, 2, 0, 1, 1)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.graphics_view_4 = QGraphicsView(self.centralwidget, window=self, id=4)
        self.graphics_view_4.setObjectName("graphics_view_4")
        self.verticalLayout_4.addWidget(self.graphics_view_4)
        self.gridLayout.addLayout(self.verticalLayout_4, 2, 1, 1, 1)
        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 1029, 22))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuResult = QtWidgets.QMenu(self.menubar)
        self.menuResult.setObjectName("menuResult")
        MainWindow.setMenuBar(self.menubar)
        self.actionOpen_all = QtWidgets.QAction(MainWindow)
        self.actionOpen_all.setObjectName("actionOpen_all")
        self.actionOpen_first = QtWidgets.QAction(MainWindow)
        self.actionOpen_first.setObjectName("actionOpen_first")
        self.actionOpen_second = QtWidgets.QAction(MainWindow)
        self.actionOpen_second.setObjectName("actionOpen_second")
        self.actionOpen_third = QtWidgets.QAction(MainWindow)
        self.actionOpen_third.setObjectName("actionOpen_third")
        self.actionOpen_fourth = QtWidgets.QAction(MainWindow)
        self.actionOpen_fourth.setObjectName("actionOpen_fourth")
        self.result_action_table = QtWidgets.QAction(MainWindow)
        self.result_action_table.setObjectName("result_action_table")
        self.menuFile.addAction(self.actionOpen_all)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionOpen_first)
        self.menuFile.addAction(self.actionOpen_second)
        self.menuFile.addAction(self.actionOpen_third)
        self.menuFile.addAction(self.actionOpen_fourth)
        self.menuResult.addAction(self.result_action_table)
        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuResult.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuResult.setTitle(_translate("MainWindow", "Result"))
        self.actionOpen_all.setText(_translate("MainWindow", "Open all"))
        self.actionOpen_first.setText(_translate("MainWindow", "Open first"))
        self.actionOpen_second.setText(_translate("MainWindow", "Open second"))
        self.actionOpen_third.setText(_translate("MainWindow", "Open third"))
        self.actionOpen_fourth.setText(_translate("MainWindow", "Open fourth"))
        self.result_action_table.setText(_translate("MainWindow", "Result table"))
