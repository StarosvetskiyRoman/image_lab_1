from PyQt5.QtWidgets import QMainWindow, QTableWidget, QTableWidgetItem, QDesktopWidget

from services.CoOccurrenceTableService import CoOccurrenceTableService
from services.ImageContrastService import ImageContrastService
from services.ImageEnergyService import ImageEnergyService
from services.ImageHomohenService import ImageHomohenService
from services.InitialTableService import InitialTableService
from services.NormalizedTableService import NormalizedTableService
from views import matrix_window


class MatrixWindow(QMainWindow, matrix_window.Ui_MainWindow):

    initial_table_service = InitialTableService
    co_occurrence_table_service = CoOccurrenceTableService
    normalized_table_service = NormalizedTableService

    energy_service = ImageEnergyService
    contrast_service = ImageContrastService
    homogen_service = ImageHomohenService

    def __init__(self, parent, *args, **kwargs):
        super().__init__()
        self.setupUi(self)
        self.number = kwargs.get('number')
        self.setWindowTitle(f'Data {self.number} image')
        self.parent_ = parent
        self.set_window_parameters()

        self.initial_matrix = []
        self.co_occurrence_matrix = []
        self.normalized_matrix = []

        self.energy = 0
        self.contrast = 0
        self.homohen = 0

    def calculate(self):
        path = self.parent_.images_paths[self.number]

        self.initial_matrix = self.initial_table_service(path).get_result_matrix()
        self.display_matrix(self.initial_matrix, self.initial_table_widget)

        self.co_occurrence_matrix = self.co_occurrence_table_service(path).get_result_matrix()
        self.display_matrix(self.co_occurrence_matrix, self.co_occurrence_table_widget)

        self.normalized_matrix = self.normalized_table_service(path).use_prepared_matrix(self.co_occurrence_matrix)\
            .get_result_matrix()
        self.display_matrix(self.normalized_matrix, self.normalized_table_widget)

        self.calculate_meanings()

    def calculate_meanings(self):
        self.energy = self.energy_service(self.normalized_matrix).get_meaning()
        self.contrast = self.contrast_service(self.normalized_matrix).get_meaning()
        self.homohen = self.homogen_service(self.normalized_matrix).get_meaning()

    def show(self):
        super().show()
        self.calculate()

    def set_table_size(self, size: tuple, table: QTableWidget):
        col, row = size
        table.setRowCount(row)
        table.setColumnCount(col)

    def collapse_table(self, table: QTableWidget):
        table.setColumnCount(0)
        table.setRowCount(0)

    def display_matrix(self, matrix, table_widget: QTableWidget):
        self.collapse_table(table_widget)
        self.set_table_size((len(matrix[0]), len(matrix)), table_widget)
        for row_index, row in enumerate(matrix):
            for col_index, element in enumerate(row):
                item = QTableWidgetItem(str(element))
                table_widget.setItem(row_index, col_index, item)
        table_widget.resizeColumnsToContents()

    def set_window_parameters(self):
        point_position_map = {
            1: 'topLeft',
            2: 'topRight',
            3: 'bottomLeft',
            4: 'bottomRight',
        }
        qt_rect = self.frameGeometry()
        point = getattr(QDesktopWidget().availableGeometry(), point_position_map[self.number])()
        self.resize(QDesktopWidget().availableGeometry().width() // 2, QDesktopWidget().availableGeometry().height() // 2)
        qt_rect.moveCenter(point)
        self.move(qt_rect.topLeft())
