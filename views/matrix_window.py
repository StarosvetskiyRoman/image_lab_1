# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui_sources/matrix_window.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.tab2 = QtWidgets.QTabWidget(self.centralwidget)
        self.tab2.setObjectName("tab2")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.tab)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.initial_table_widget = QtWidgets.QTableWidget(self.tab)
        self.initial_table_widget.setObjectName("initial_table_widget")
        self.initial_table_widget.setColumnCount(0)
        self.initial_table_widget.setRowCount(0)
        self.gridLayout_2.addWidget(self.initial_table_widget, 0, 0, 1, 1)
        self.tab2.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.gridLayout_4 = QtWidgets.QGridLayout(self.tab_2)
        self.gridLayout_4.setObjectName("gridLayout_4")
        self.co_occurrence_table_widget = QtWidgets.QTableWidget(self.tab_2)
        self.co_occurrence_table_widget.setObjectName("co_occurrence_table_widget")
        self.co_occurrence_table_widget.setColumnCount(0)
        self.co_occurrence_table_widget.setRowCount(0)
        self.gridLayout_4.addWidget(self.co_occurrence_table_widget, 0, 0, 1, 1)
        self.tab2.addTab(self.tab_2, "")
        self.tab_3 = QtWidgets.QWidget()
        self.tab_3.setObjectName("tab_3")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.tab_3)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.normalized_table_widget = QtWidgets.QTableWidget(self.tab_3)
        self.normalized_table_widget.setObjectName("normalized_table_widget")
        self.normalized_table_widget.setColumnCount(0)
        self.normalized_table_widget.setRowCount(0)
        self.gridLayout_3.addWidget(self.normalized_table_widget, 0, 0, 1, 1)
        self.tab2.addTab(self.tab_3, "")
        self.gridLayout.addWidget(self.tab2, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        self.tab2.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Matrix window"))
        self.tab2.setTabText(self.tab2.indexOf(self.tab), _translate("MainWindow", "Initial"))
        self.tab2.setTabText(self.tab2.indexOf(self.tab_2), _translate("MainWindow", "Co-occurrence"))
        self.tab2.setTabText(self.tab2.indexOf(self.tab_3), _translate("MainWindow", "Normalized"))

