from typing import Iterable

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMainWindow, QTableWidgetItem

from views import result_matrix
from views.MatrixWindow import MatrixWindow


class ResultWindow(QMainWindow, result_matrix.Ui_MainWindow):

    COMPARE_WITH_IMAGE_NUMBER = 4

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.setupUi(self)
        self.windows = None
        self.setWindowTitle('Result matrix window')

    def set_windows(self, windows: Iterable[MatrixWindow]):
        self.windows = windows

    def get_windows_values(self):
        if self.windows:
            evclid_values = self.get_evclid_values()
            return [(win.energy, win.contrast, win.homohen, evclid_value)
                    for win, evclid_value in zip(self.windows, evclid_values)]

    def get_evclid_values(self):
        def _calculate_formula(compare, compare_with):
            c_en, c_co, c_ho = compare
            cw_en, cw_co, cw_ho = compare_with
            return pow(pow(c_en - cw_en, 2) + pow(c_co - cw_co, 2) + pow(c_ho - cw_ho, 2), 0.5)
        windows = list(self.windows)
        evclid_values = []
        ind = self.COMPARE_WITH_IMAGE_NUMBER - 1
        for window in windows:
            evclid_values.append(_calculate_formula(
                (window.energy, window.contrast, window.homohen),
                (windows[ind].energy, windows[ind].contrast, windows[ind].homohen)
            ))
        return evclid_values

    def show(self):
        if self.windows:
            super().show()
            self.display_table()

    def display_table(self):
        values = self.get_windows_values()
        for row_index, row in enumerate(values):
            for column_index, element_value in enumerate(row):
                item = QTableWidgetItem(str(element_value))
                self.tableWidget.setItem(row_index, column_index, item)
        self.tableWidget.setSizeAdjustPolicy(QtWidgets.QAbstractScrollArea.AdjustToContents)
        self.tableWidget.resizeColumnsToContents()
        self.resize(self.sizeHint())
