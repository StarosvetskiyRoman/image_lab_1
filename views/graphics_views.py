from PyQt5 import QtWidgets


class QGraphicsView(QtWidgets.QGraphicsView):

    def __init__(self, *args, window=None, id=0):
        super().__init__(*args)
        self.window_ = window
        self.id = id

    def mouseDoubleClickEvent(self, QMouseEvent):
        self.window_.display_data(self.id)
