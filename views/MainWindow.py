import os
import sys
from functools import partial

from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QMainWindow, QApplication, QFileDialog, QGraphicsScene, QDesktopWidget

from views import main_window
from views.MatrixWindow import MatrixWindow
from views.ResultMatrix import ResultWindow


class MainWindow(QMainWindow, main_window.Ui_MainWindow):

    core_directory = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), 'media/')

    def __init__(self, *args, **kwargs):
        super().__init__()
        self.setupUi(self)

        qt_rect = self.frameGeometry()
        centerPoint = QDesktopWidget().availableGeometry().center()
        qt_rect.moveCenter(centerPoint)
        self.move(qt_rect.topLeft())

        self.first_matrix_window = MatrixWindow(self, number=1)
        self.second_matrix_window = MatrixWindow(self, number=2)
        self.third_matrix_window = MatrixWindow(self, number=3)
        self.fourth_matrix_window = MatrixWindow(self, number=4)

        self.actionOpen_all.triggered.connect(partial(self.open_file_dialog, multiple=True))
        self.actionOpen_first.triggered.connect(partial(self.open_file_dialog, number=1))
        self.actionOpen_second.triggered.connect(partial(self.open_file_dialog, number=2))
        self.actionOpen_third.triggered.connect(partial(self.open_file_dialog, number=3))
        self.actionOpen_fourth.triggered.connect(partial(self.open_file_dialog, number=4))
        self.result_action_table.triggered.connect(self.display_result)

        self.result = ResultWindow(self)

        self.images_paths = {
            1: '',
            2: '',
            3: '',
            4: '',
        }

    def open_file_dialog(self, **kwargs):
        print(self.core_directory)
        try:
            if kwargs.get('multiple', False):
                images_paths, _ = QFileDialog.getOpenFileNames(
                    parent=self, caption='Open all files', directory=self.core_directory)
                for pos, image_path in enumerate(images_paths[:4]):
                    self.set_image(image_path, num=pos + 1)
            else:
                num = kwargs.get('number', 1)
                image_path, _ = QFileDialog.getOpenFileName(
                    parent=self, caption=f'Open file {num}', directory=self.core_directory)
                self.set_image(image_path, num=num)
        except Exception as exc:
            print(str(exc))

    def display_result(self):
        windows_instances = (self.first_matrix_window,
                             self.second_matrix_window,
                             self.third_matrix_window,
                             self.fourth_matrix_window)
        if all([all([window_.initial_matrix, window_.co_occurrence_matrix, window_.normalized_matrix]) for window_ in windows_instances]):
            self.result.set_windows(windows_instances)
            self.result.show()

    def display_data(self, number: int = 1):
        if self.images_paths[number]:
            map = {
                1: 'first',
                2: 'second',
                3: 'third',
                4: 'fourth'
            }
            getattr(self, f'{map[number]}_matrix_window').show()

    def set_image(self, path: str, num: int = 1):
        self.images_paths[num] = path
        scene = QGraphicsScene()
        pixmap = QPixmap(path)
        pixmap = pixmap.scaled(200, 200)
        scene.addPixmap(pixmap)
        getattr(self, f'graphics_view_{num}').setScene(scene)


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    app.exec()
