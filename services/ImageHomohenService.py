from services.abstract import AbstractImageMeaningService


class ImageHomohenService(AbstractImageMeaningService):

    def cell_data_perform_action(self, i, j):
        return self.matrix[i][j] / (1 + abs(i - j))
