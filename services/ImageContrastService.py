from services.abstract import AbstractImageMeaningService


class ImageContrastService(AbstractImageMeaningService):

    def cell_data_perform_action(self, i, j):
        return pow(i - j, 2) * self.matrix[i][j]
