from services.abstract import AbstractImageMeaningService


class ImageEnergyService(AbstractImageMeaningService):

    def cell_data_perform_action(self, i, j):
        return pow(self.matrix[i][j], 2)
