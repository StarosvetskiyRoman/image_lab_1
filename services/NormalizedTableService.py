from services.abstract import AbstractImageService


class NormalizedTableService(AbstractImageService):

    def update_image_data(self, matrix):
        matrix_sum = self.get_matrix_sum(matrix)
        normalized_matrix = [[el / matrix_sum for y, el in enumerate(row_array)] for x, row_array in enumerate(matrix)]

        return normalized_matrix

    def get_matrix_sum(self, matrix):
        return sum([sum(row) for row in matrix])
