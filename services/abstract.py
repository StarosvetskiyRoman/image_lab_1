from typing import List

from PIL import Image


class LinkedIndicesMixin(object):
    """
    self.width and self.height have to be declared before using
    """

    def _get_linked_indices(self, pos_x: int, pos_y: int):
        indices = list()
        indices.append((pos_x, pos_y))
        if pos_x < self.width - 1:
            indices.append((pos_x + 1, pos_y))
        if pos_x < self.width - 1 and pos_y != 0:
            indices.append((pos_x + 1, pos_y - 1))
        if pos_y != 0:
            indices.append((pos_x, pos_y - 1))
        if pos_x != 0 and pos_y != 0:
            indices.append((pos_x - 1, pos_y - 1))
        if pos_x != 0:
            indices.append((pos_x - 1, pos_y))
        if pos_x != 0 and pos_y < self.height - 1:
            indices.append((pos_x - 1, pos_y + 1))
        if pos_y < self.height - 1:
            indices.append((pos_x, pos_y + 1))
        if pos_x < self.width - 1 and pos_y < self.height - 1:
            indices.append((pos_x + 1, pos_y + 1))
        return indices


class AbstractImageService(LinkedIndicesMixin):

    def __init__(self, path):
        self.path = path
        self.matrix = None

    def use_prepared_matrix(self, matrix):
        self.matrix = matrix
        return self

    @staticmethod
    def list_to_matrix(array: list, cols: int, rows: int):
        matrix = [[array[cols * row_ind + col_ind] for col_ind in range(cols)] for row_ind in range(rows)]
        return matrix

    @staticmethod
    def matrix_to_list(matrix):
        return [el for row in matrix for el in row]

    def get_image_data(self):
        if self.matrix:
            return self.matrix
        try:
            image = Image.open(self.path)
            self.width, self.height = image.size
            halftone_data = [d if type(d) == int else d[0] for d in image.getdata()]
            return self.list_to_matrix(halftone_data, self.width, self.height)
        except FileNotFoundError:
            pass

    def get_result_matrix(self):
        return self.update_image_data(self.get_image_data())

    def update_image_data(self, matrix):
        return matrix


class AbstractImageMeaningService(LinkedIndicesMixin):

    def __init__(self, input_matrix: List[List]):
        self.matrix = input_matrix
        self.width = len(input_matrix[0])
        self.height = len(input_matrix)

    def get_meaning(self):
        image_parameter = 0

        for i, row_array in enumerate(self.matrix):
            for j, el in enumerate(row_array):
                image_parameter += self.cell_data_perform_action(i, j)

        return image_parameter

    def cell_data_perform_action(self, i, j):
        return 0
