from services.abstract import AbstractImageService


class CoOccurrenceTableService(AbstractImageService):

    PairedElementRowIndex = -1
    PairedElementColIndex = 1

    def update_image_data(self, matrix):
        co_occurrence_matrix = [[0 for _ in range(256)] for _ in range(256)]

        for i, row_array in enumerate(matrix):
            for j, el in enumerate(row_array):
                if(0 <= i + self.PairedElementRowIndex < len(matrix) and
                   0 <= j + self.PairedElementColIndex < len(matrix[i])):
                    row_index = el
                    col_index = matrix[i + self.PairedElementRowIndex][j + self.PairedElementColIndex]
                    co_occurrence_matrix[row_index][col_index] += 1

        return co_occurrence_matrix
